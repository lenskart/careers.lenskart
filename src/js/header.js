onload();
function onload() {
    let header = document.getElementById('header');
    header.innerHTML =
        `<header class="header-container pd-n pd-b2">
        <div class="header-wrapper">
            <nav class="nav display-flex justify-space-between align-center">
                <div class="logo-div mr-t-5">
                    <img alt="lenskart-logo" class="logo-img" src="../static/lenskart-logo.svg" />
                </div>
                <div class="nav-links">
                    <span class="nav-link fw500 mr-l2 fs1">Home</span>
                    <span class="nav-link fw500 mr-l2 fs1">Jobs</span>
                    <span class="nav-link fw500 mr-l2 fs1">Lenskart videos</span>
                    <span class="nav-link fw500 mr-l2 fs1">
                      <a href="https://blog.lenskart.com/peopleculture/home" target="_blank">People & Culture</a>
                    </span>
                    <span class="nav-link fw500 mr-l2 fs1">
                      <a href="https://blog.lenskart.com/lenskart-product/home" target="_blank">Engineering</a>
                    </span>
                </div>
                <img id="hamberger-menu" src="../static/hamberger.svg" class="ham-berger" alt="hamberger">
            </nav>
            <hr class="nav-division" />
            <div class="header-content display-flex justify-space-between align-center mr-t1">
                <div class="left-content display-flex align-center">
                    <div class="tag-line pd-t2 pd-b2">
                        <div class="tag-text rajdhani tag fs5"><span class="rajdhani fwBold">SEE</span> MORE<span class="rajdhani fwBold">. DO</span>
                            MORE<span class="rajdhani fwBold">. BE</span> MORE</div>
                        <div class="seconary-tag fs1 mr-b2 mr-t1">We are looking for aspiring, ambitious, passionate and smart team
                            members
                            who want to change the world.</div>
                        <div class="redirect-link display-flex">
                            <span class="explose-roles mr-r2 fs1">EXPLORE ROLES</span>
                            <div class="company-website-wrapper">
                                <span class="company-website fs1">COMPANY WEBSITE</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="right-content">
                    <img class="header-img" alt="frame-1" src="../static/Frame-1.svg" />
                </div>
            </div>
        </div>
    </header>
    <div id="hamberger-options-container" class="pos-abs">
        <nav id="nav-links-mobile" class="pd-l2">
            <div class="display-flex justify-space-between align-center pd-1-5">
                <img alt="lenskart-logo" class="logo-img" src="../static/lenskart-logo.svg" />
                <img id="close-menu" alt="close icon" class="close-img" src="../static/close-icon.svg" />
            </div>
            <div class="pd-b2">
                <h3 class="nav-link fw500 fs2-4 mr-t1-5 mr-l2 fs1">Home</h3>
                <h3 class="nav-link fw500 fs2-4 mr-t1-5 mr-l2 fs1">Jobs</h3>
                <h3 class="nav-link fw500 fs2-4 mr-t1-5 mr-l2 fs1">Lenskart videos</h3>
                <h3 class="nav-link fw500 fs2-4 mr-t1-5 mr-l2 fs1">
                  <a href="https://blog.lenskart.com/peopleculture/home" target="_blank">People & Culture</a>
                </h3>
                <h3 class="nav-link fw500 fs2-4 mr-t1-5 mr-l2 fs1">
                  <a href="https://blog.lenskart.com/lenskart-product/home" target="_blank">Engineering</a>
                </h3>
            </div>       
        </nav>
    </div>
    `
}

const hambergerMenu = document.getElementById('hamberger-menu');
const closeMenuIcon = document.getElementById('close-menu');
const hambergerOptionsContainer = document.getElementById('hamberger-options-container');
const bodyContainer = document.getElementById('body-container');

const closeMenu = ()=> {
    hambergerOptionsContainer.style.display = 'none';
    bodyContainer.style.overflowY = 'auto';
}

hambergerMenu.addEventListener('click', ()=>{
    hambergerOptionsContainer.style.display = 'block';
    bodyContainer.style.overflowY = 'hidden';
});

hambergerOptionsContainer.addEventListener('click',(e)=> {
    if(e.target.id === 'hamberger-options-container'){
        closeMenu();
    }
})
closeMenuIcon.addEventListener('click',closeMenu);

window.addEventListener('resize',()=>{
    if(window.innerWidth > 615 && hambergerOptionsContainer.style.display === 'block'){
        closeMenu();
    }
})