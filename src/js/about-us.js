onload();
function onload() {
    let aboutUs = document.getElementById('about-us');
    aboutUs.innerHTML =
        `<section class="about-us-conatiner pd-n">
        <!-- OUR LEADERBOARD -->
        <section class="our-leaderboard pd-t5">
            <div class="leaderboard-heading-div display-flex flex-column">
                <div class="display-flex align-center">
                    <span class="leadership section-heading pd-b1 fs2 mulish">Our Leadership</span>
                    <hr class="supporter mr-l1"></hr>
                </div>
                <p class="subsection-heading fs1-2 mulish">Sed ut perspiciatis unde omnis iste natuem  totam rem aperiam, eaque ipsarchitecto</p>
            </div>
            <div class="about-us-details mr-t2 pd-b5">
                <div class="ppl-details display-flex justify-space-around align-item">
                    <!-- MSITE -->
                    <div class="m-site-about-us mr-b7 display-flex justify-space-around flex-column">
                        <div class="user-img"></div>
                        <div class="msite-info display-flex justify-space-around flex-column pd-t1 pd-b1">
                            <span class="mulish fs2 fwBold">Summet Kapahi</span>
                            <span class="msite-designation mulish fs1-2 fw500">Co-Founder & Head Sourcing</span>
                            <img class="msite-linkedin fs2" src="../static/msite-linkedin.svg" alt="linkedin">
                        </div>
                    </div>
                    <div class="m-site-about-us mr-b7 display-flex justify-space-around flex-column">
                        <div class="user-img"></div>
                        <div class="msite-info display-flex justify-space-around flex-column pd-t1 pd-b1">
                            <span class="mulish fs2 fwBold">Summet Kapahi</span>
                            <span class="msite-designation mulish fs1-2 fw500">Co-Founder & Head Sourcing</span>
                            <img class="msite-linkedin pd-t1 pd-b1 fs2" src="../static/msite-linkedin.svg" alt="linkedin">
                        </div>
                    </div>
                    <div class="m-site-about-us mr-b7 display-flex justify-space-around flex-column">
                        <div class="user-img"></div>
                        <div class="msite-info display-flex justify-space-around flex-column pd-t1 pd-b1">
                            <span class="mulish fs2 fwBold">Summet Kapahi</span>
                            <span class="msite-designation mulish fs1-2 fw500">Co-Founder & Head Sourcing</span>
                            <img class="msite-linkedin pd-t1 pd-b1 fs2" src="../static/msite-linkedin.svg" alt="linkedin">
                        </div>
                    </div>
                    <div class="m-site-about-us mr-b7 display-flex justify-space-around flex-column">
                        <div class="user-img"></div>
                        <div class="msite-info display-flex justify-space-around flex-column pd-t1 pd-b1">
                            <span class="mulish fs2 fwBold">Summet Kapahi</span>
                            <span class="msite-designation mulish fs1-2 fw500">Co-Founder & Head Sourcing</span>
                            <img class="msite-linkedin pd-t1 pd-b1 fs2" src="../static/msite-linkedin.svg" alt="linkedin">
                        </div>
                    </div>
                    <div class="m-site-about-us mr-b7 display-flex justify-space-around flex-column">
                        <div class="user-img"></div>
                        <div class="msite-info display-flex justify-space-around flex-column pd-t1 pd-b1">
                            <span class="mulish fs2 fwBold">Summet Kapahi</span>
                            <span class="msite-designation mulish fs1-2 fw500">Co-Founder & Head Sourcing</span>
                            <img class="msite-linkedin pd-t1 pd-b1 fs2" src="../static/msite-linkedin.svg" alt="linkedin">
                        </div>
                    </div>
                    <!-- DESKTOP -->
                    <div class="about-us-info mr-t5 display-flex align-center pos-rel">
                        <div class="user-img-wrapper pos-abs"></div>
                        <span class="fs1 fwBold mr-t2">Peyush Bansal</span>
                        <span class="fs0-8 text-center designation">Founder, CEO</span>
                        <img src="../static/linked-in.svg" alt="linkedin">
                    </div>
                    <div class="about-us-info mr-t5 display-flex align-center pos-rel">
                        <div class="user-img-wrapper pos-abs"></div>
                        <span class="fs1 fwBold mr-t2">Amith chaudary</span>
                        <span class="fs0-8 text-center designation">Co-Founder & Cheif Innovation Officer</span>
                        <img src="../static/linked-in.svg" alt="linkedin">
                    </div>
                    <div class="about-us-info mr-t5 display-flex align-center pos-rel">
                        <div class="user-img-wrapper pos-abs"></div>
                        <span class="fs1 fwBold mr-t2">Summet Kapahi</span>
                        <span class="fs0-8 text-center designation">Co-Founder & Head sourcing</span>
                        <img src="../static/linked-in.svg" alt="linkedin">
                    </div>
                    <div class="about-us-info mr-t5 display-flex align-center pos-rel">
                        <div class="user-img-wrapper pos-abs"></div>
                        <span class="fs1 fwBold mr-t2">Neha Bansal</span>
                        <span class="fs0-8 text-center designation">Co-Founder</span>
                        <img src="../static/linked-in.svg" alt="linkedin">
                    </div>
                    <div class="about-us-info mr-t5 display-flex align-center pos-rel">
                        <div class="user-img-wrapper pos-abs"></div>
                        <span class="fs1 fwBold mr-t2">Ramneek Khuranna</span>
                        <span class="fs0-8 text-center designation">Co-Founder & Vice president</span>
                        <img src="../static/linked-in.svg" alt="linkedin">
                    </div>
                </div>
            </div>
        </section>
        <!-- DEVIDER -->

        <p class="section-divider">
            ////////////////////////////////////////////////////////////////
        </p>
        <!-- LENSKART JOURNEY -->
        <section class="lenskart-journey-wrapper pd-t5 pd-b2">
            <div class="lenskart-journey-div display-flex align-center">
                <div class="display-flex align-center"></div>
                    <span class="journey section-heading pd-b1 fs2 mulish">Lenskart Journey</span>
                    <hr class="supporter mr-l1"></hr>
                </div>
                <p class="subsection-heading fs1-2 mulish">Sed ut perspiciatis unde omnis iste natuem  totam rem aperiam, eaque ipsarchitecto</p>
            </div>
            <!-- <div class="graph-wrapper display-flex"> -->
                <div class="graph-wrapper display-flex justify-space-between align-center mr-t5 pos-rel">
                    <img src="../static/graph.svg" alt="graph" class="graph pos-abs pd-l7-5 mr-b5 mr-t5">
                    <span class="column-divider"></span>
                    <div class="single-column flex-column display-flex justify-space-between align-center">
                        <span class="stage-text text-center fs1-2 fwBold">Lenskart Launch</span>
                        <span class="fwBold fs1-4">2010</span>
                    </div>
                    <span class="column-divider"></span>
                    <div class="single-column flex-column display-flex justify-space-between align-center">
                        <span class="stage-text text-center fs1-2 fwBold">Offline Store </span>
                        <span class="fwBold fs1-4">2012</span>
                    </div>
                    <span class="column-divider"></span>
                    <div class="single-column flex-column display-flex justify-space-between align-center">
                        <span class="stage-text text-center fs1-2 fwBold">John Jacobs Launch</span>
                        <span class="fwBold fs1-4">2014</span>
                    </div>
                    <span class="column-divider"></span>
                    <div class="single-column flex-column display-flex justify-space-between align-center">
                        <span class="stage-text text-center fs1-2 fwBold">Manufacturing Unit</span>
                        <span class="fwBold fs1-4">2017</span>
                    </div>
                    <span class="column-divider"></span>
                    <div class="single-column flex-column display-flex justify-space-between align-center">
                        <span class="stage-text text-center fs1-2 fwBold">Numero UNO</span>
                        <span class="fwBold text-center fs1-4">2020-2021</span>
                    </div>
                    <span class="column-divider"></span>
                </div>
            <!-- </div> -->
        </section>
    </section>
    <section class="lenskart-mislleneous-container">
        <div class="display-flex align-center pd-n mislleneous-title-containner"> 
            <p class="title fs1-2 blackish">Lenskart Miscellaneous</p>
            <div class="horizontal-line mr-l1"></div>
        </div>
        <p class="purpose-dna-para pd-n mr-b5"><q>We exist to transform the way people see and experience the world</q></p>
        <div id="mislleneous-desktop">
        <div class="display-flex card-list-mislleneous pd-n pd-b2">
                <div class="card-mislleneous">
                  <div class="mislleneous-img"></div>
                  <h3>Lenskart Vision Fund</h3>
                  <p>Sed ut perspiciatis unde omnis of totam rem aperiam eaq ipsarchitec set igdt trsiom nuaytsk ki.</p>
                </div>
                <div class="card-mislleneous">
                  <div class="mislleneous-img"></div>
                  <h3>Lenskart Foundation</h3>
                  <p>Sed ut perspiciatis unde omnis of totam rem aperiam eaq ipsarchitec set igdt trsiom nuaytsk ki.</p>
                </div>
                <div class="card-mislleneous">
                  <div class="mislleneous-img"></div>
                  <h3>Lenskart Manufacturing Plant</h3>
                  <p>Sed ut perspiciatis unde omnis of totam rem aperiam eaq ipsarchitec set igdt trsiom nuaytsk ki.</p>
                </div>
                <div class="card-mislleneous">
                  <div class="mislleneous-img"></div>
                  <h3>Lenskart Academy</h3>
                  <p>Sed ut perspiciatis unde omnis of totam rem aperiam eaq ipsarchitec set igdt trsiom nuaytsk ki.</p>
                </div>
        </div>
        <div class="cards-back-background"></div>
        <div>
    </section>
    <div id="mislleneous-mobile" class="slider-mislleneous pd-n pd-b2">
        <div class="mislleneous-card-mobile">
            <div class="mislleneous-img-mobile"></div>
            <h3>Lenskart Vision Fund</h3>
            <p>Sed ut perspiciatis unde omnis of totam rem aperiam eaq ipsarchitec set igdt trsiom nuaytsk ki.</p>
        </div>
        <div class="mislleneous-card-mobile">
            <div class="mislleneous-img-mobile"></div>
            <h3>Lenskart Foundation</h3>
            <p>Sed ut perspiciatis unde omnis of totam rem aperiam eaq ipsarchitec set igdt trsiom nuaytsk ki.</p>
        </div>
        <div class="mislleneous-card-mobile">
            <div class="mislleneous-img-mobile"></div>
            <h3>Lenskart Manufacturing Plant</h3>
            <p>Sed ut perspiciatis unde omnis of totam rem aperiam eaq ipsarchitec set igdt trsiom nuaytsk ki.</p>
        </div>
        <div class="mislleneous-card-mobile">
            <div class="mislleneous-img-mobile"></div>
            <h3>Lenskart Academy</h3>
            <p>Sed ut perspiciatis unde omnis of totam rem aperiam eaq ipsarchitec set igdt trsiom nuaytsk ki.</p>
        </div>
    </div>
    `
}

$('.slider-mislleneous').slick(
    {infinite: true,
	dots: true,
	arrows: false,
	autoplay: true,
	autoplaySpeed: 3000,
	fade: true,
	fadeSpeed: 1000
});


