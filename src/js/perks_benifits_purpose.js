onload();
function onload() {
    let perkBenifitPurpose = document.getElementById('perk-benifit-purpose');
    perkBenifitPurpose.innerHTML =
        `
        <section class="pd-n">
        <section id="perks-benifts" class="mulish fwBold">
            <div class="display-flex align-center justify-space-between mr-b5">
                <div class="display-flex align-center"> 
                <p class="title fs1-2 mr-0 blackish">Perks & benifits</p>
                <div class="horizontal-line mr-l1"></div>
                </div>
            </div>
            <div>
                <div class="display-flex row">
                <div class="perk-benifit-item fw400 fs1-2 display-flex mr-r4">
                    <div class="point-circle"></div>
                    <div class="mr-l1">
                        <h3>Fast Growth</h3>
                        <p class="perk-benifit-item-para mr-t1 mr-b4">Sed ut perspiciatis unde omnis of totam rem aperiam.</p>
                    </div>
                    </div>
                    <div class="perk-benifit-item display-flex mr-r4">
                    <div class="point-circle"></div>
                    <div class="mr-l1">
                        <h3>No Leave Policy</h3>
                        <p class="perk-benifit-item-para mr-t1 mr-b4">Sed ut perspiciatis unde omnis of totam rem aperiam.</p>
                    </div>
                    </div>
                </div>
                <div class="display-flex row">
                <div class="perk-benifit-item fw400 fs1-2 display-flex mr-r4">
                    <div class="point-circle"></div>
                    <div class="mr-l1">
                        <h3>Health Insurance</h3>
                        <p class="perk-benifit-item-para mr-t1 mr-b4">Sed ut perspiciatis unde omnis of totam rem aperiam.</p>
                    </div>
                </div>
                <div class="perk-benifit-item fw400 fs1-2 display-flex mr-r4">
                    <div class="point-circle"></div>
                    <div class="mr-l1"> 
                        <h3>Work from Home</h3>
                        <p class="perk-benifit-item-para mr-t1 mr-b4">Sed ut perspiciatis unde omnis of totam rem aperiam.</p>
                    </div>
                </div>
                </div>
                <div class="display-flex row">
                    <div class="perk-benifit-item fw400 fs1-2 display-flex mr-r4">
                    <div class="point-circle"></div>
                    <div class="mr-l1">
                        <h3>Relaxed Dress Code</h3>
                        <p class="perk-benifit-item-para mr-t1 mr-b4">Sed ut perspiciatis unde omnis of totam rem aperiam.</p>
                    </div>
                    </div>
                    <div class="perk-benifit-item fw400 fs1-2 display-flex mr-r4 not-border">
                    <div class="point-circle"></div>
                    <div class="mr-l1">
                        <h3>Highest Levels of Learnings</h3>
                        <p class="perk-benifit-item-para mr-t1 mr-b4">Sed ut perspiciatis unde omnis of totam rem aperiam.</p>
                    </div>
                    </div>
                </div>
            </div>
        </section>
        <span class="section-divider pink">
            ////////////////////////////////////////////////////////////////
        </span>
        <section id="purpose-dna" class="mr-t2 fwBold mulish">
            <div class="display-flex align-center"> 
                <p class="title fs1-2 blackish">Our Purpose and DNA</p>
                <div class="horizontal-line mr-l1"></div>
            </div>
            <p class="purpose-dna-para mr-t-5 fs1 pd-b4"><q>We exist to transform the way people see and experience the world</q></p>
            <div class="display-flex justify-center card-list">
                <div class="card puropse-dna-card mr-t2">
                    <img src="../static/Flash.svg" alt="flash"/>
                    <h3 class="fs1-4 mr-0 pd-r1 fwBold card-heading">Start-up Energy</h3>
                    <p class="mr-0 mr-t1 fs1 pd-b3 pd-r1 card-para">Eos tota dicuntdemo critum no. Has nat Est viris soleat sadipscing cu. Lorem ipsum dolor.</p>
                </div>
                <div class="card puropse-dna-card mr-t2">
                    <img src="../static/Trophy.svg" alt="flash"/>
                    <h3 class="fs1-4 mr-0 pd-r1 fwBold card-heading">10x Thinking</h3>
                    <p class="mr-0 mr-t1 fs1 pd-b3 pd-r1 card-para">Eos tota dicuntdemo critum no. Has nat Est viris soleat sadipscing cu. Lorem ipsum dolor.</p>
                </div>
                <div class="card puropse-dna-card mr-t2">
                    <img src="../static/Key.svg" alt="flash"/>
                    <h3 class="fs1-4 mr-0 pd-r1 fwBold card-heading">Fail Harder</h3>
                    <p class="mr-0 mr-t1 fs1 pd-b3 pd-r1 card-para">Eos tota dicuntdemo critum no. Has nat Est viris soleat sadipscing cu. Lorem ipsum dolor.</p>
                </div>
                <div class="card puropse-dna-card mr-t2">
                    <img src="../static/Star.svg" alt="flash"/>
                    <h3 class="fs1-4 mr-0 pd-r1 fwBold card-heading">Customer & Team Obession</h3>
                    <p class="mr-0 mr-t1 fs1 pd-b3 pd-r1 card-para">Eos tota dicuntdemo critum no. Has nat Est viris soleat sadipscing cu. Lorem ipsum dolor.</p>
                </div>
                <div class="card puropse-dna-card mr-t2">
                    <img src="../static/Crown.svg" class="crown-img" alt="flash"/>
                    <h3 class="fs1-4 mr-0 pd-r1 fwBold card-heading">Collaborative Ownership</h3>
                    <p class="mr-0 mr-t1 fs1 pd-b3 pd-r1 card-para">Eos tota dicuntdemo critum no. Has nat Est viris soleat sadipscing cu. Lorem ipsum dolor.</p>
                </div>
            </div>
        </section>
    </section>
    <div id="purpose-dna-modal-container" class="pd-n">
        <div id="purpose-dna-modal">
          <span id="purpose-dna-close-btn"><img src="../static/close-icon.svg" class="close-icon" alt="flash"/></span>
          <div class="display-flex purpose-dna-modal-top">
              <img id="purpose-dna-modal-img" src="../static/Flash.svg"/>
              <div class="mr-l1">
                 <span class="fs1 pd-b2 fwBold">Our purpose and DNA</span>
                 <h3 class="mr-t-5 fw500 fs1-5" id="purpose-dna-title">
                 </h3>
                 <ol type="1" id="purpose-dna-list">
                 </ol>  
              </div>
          </div>
        </div>
    </div>
        `

}

const purposeDnaContainer = document.getElementById('purpose-dna');
const purposednaModalImg = document.getElementById('purpose-dna-modal-img');
const purposeDnaCards = document.getElementsByClassName('puropse-dna-card');
const puropseDnaModalContainer = document.getElementById('purpose-dna-modal-container');
const purposeDnaCloseBtn = document.getElementById('purpose-dna-close-btn');
const purposeDnaTitle = document.getElementById('purpose-dna-title');
const purposeDnaList = document.getElementById('purpose-dna-list');
const purposeDnaCardTitle1 = 'Start-up Energy';
const purposeDnaCardContent1 = `<li>We think positive, inspire, and never quit</li>
                                <li>Take immediate action and drive end to end results</li>
                                <li>Spot key opportunities, act fast, and grab them</li>
                                <li>Learn at a rapid pace and set new standards for yourselves and others</li>
                                <li>Be ever curious and constantly learn from others</li>
                                <li>We value agility and not just hustle</li>
                               `;
const purposeDnaCardTitle2 = '10x Thinking';
const purposeDnaCardContent2 = `<li>We think positive, inspire, and never quit</li>
                                <li>Take immediate action and drive end to end results</li>
                                <li>Spot key opportunities, act fast, and grab them</li>
                                <li>Learn at a rapid pace and set new standards for yourselves and others</li>
                              `;
const purposeDnaCardTitle3 = 'Fail Harder';
const purposeDnaCardContent3 = `<li>We think positive, inspire, and never quit</li>
                                 <li>Take immediate action and drive end to end results</li>
                                 <li>Spot key opportunities, act fast, and grab them</li>
                                 <li>Learn at a rapid pace and set new standards for yourselves and others</li>
                              `;
const purposeDnaCardTitle4 = 'Customer & Team Obession';
const purposeDnaCardContent4 = `<li>We think positive, inspire, and never quit</li>
                                <li>Take immediate action and drive end to end results</li>
                                <li>Spot key opportunities, act fast, and grab them</li>
                                <li>Learn at a rapid pace and set new standards for yourselves and others</li>
                                `;
const purposeDnaCardTitle5 = 'Collaborative Ownership';
const purposeDnaCardContent5 = `<li>We think positive, inspire, and never quit</li>
                                <li>Take immediate action and drive end to end results</li>
                                <li>Spot key opportunities, act fast, and grab them</li>
                                <li>Learn at a rapid pace and set new standards for yourselves and others</li>
                                `;


for(let i=0;i<purposeDnaCards.length;i++){
      purposeDnaCards[i].addEventListener('click', ()=>{
          document.documentElement.style.scrollBehavior = 'auto';
          document.body.scrollTop = 0; // For Safari
          document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
          bodyContainer.style.overflowY = 'hidden';
          puropseDnaModalContainer.style.display = 'block';
          switch(i) {
            case 0:
              purposednaModalImg.setAttribute('src','../static/Flash.svg');
              purposeDnaTitle.innerHTML = purposeDnaCardTitle1;
              purposeDnaList.innerHTML = purposeDnaCardContent1;
              break;
            case 1:
              purposednaModalImg.setAttribute('src','../static/Trophy.svg');
              purposeDnaTitle.innerHTML = purposeDnaCardTitle2;
              purposeDnaList.innerHTML = purposeDnaCardContent2;
              break;
            case 2:
              purposednaModalImg.setAttribute('src','../static/Key.svg');
              purposeDnaTitle.innerHTML = purposeDnaCardTitle3;
              purposeDnaList.innerHTML = purposeDnaCardContent3;
              break;
            case 3:
              purposednaModalImg.setAttribute('src','../static/Star.svg');
              purposeDnaTitle.innerHTML = purposeDnaCardTitle4;
              purposeDnaList.innerHTML = purposeDnaCardContent4;
              break;
            case 4:
              purposednaModalImg.setAttribute('src','../static/Crown.svg');
              purposeDnaTitle.innerHTML = purposeDnaCardTitle5;
              purposeDnaList.innerHTML = purposeDnaCardContent5;
              break;
          }
      })
}

const closePuropseDnaModal = ()=> {
    bodyContainer.style.overflowY = 'auto';
    puropseDnaModalContainer.style.display = 'none';
    document.documentElement.scrollTop = purposeDnaContainer.offsetTop; 
}

puropseDnaModalContainer.addEventListener('click',(e)=> {
    if(e.target.id === 'purpose-dna-modal-container'){
        closePuropseDnaModal();
    }
})
purposeDnaCloseBtn.addEventListener('click',closePuropseDnaModal)