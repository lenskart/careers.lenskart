onload();
function onload() {
    let footer = document.getElementById('Aboard');
    footer.innerHTML =
        `<section id="aboard-section">
        <div class="nav-wrapper">   
          <nav class="nav-fix display-flex justify-space-evenly align-center">
              <ul class="nav-links">
                  <li><a href="#Aboard">Welcome Aboard</a></li>
                  <li><a href="#perks-benifts">Perks And Benefits</a></li>
                  <li><a href="#about-us">About Us</a></li>
                  <li><a href="#">Gallery</a></li>
              </ul>
              <div class="explore-links">
                  <a href="#">EXPLORE OPPORTUNITIES</a>
              </div>
          </nav>
      </div>  
      <div class="aboard-bottom display-flex pd-r5 pd-l5 pd-t5 pd-b5 justify-space-evenly">
          <div class="aboard-bottom-left display-flex">
              <span class="left-text fs5">Welcome Aboard Our Rocketship</span>
              <img src="../static/Saly-1.svg" alt="rocket-img" class="align-self"/>
          </div>
          <div class="aboard-bottom-right display-flex justify-space-between flex-column">
              <div class="box pd-2 fs1-5 text-center mr-r5">
                  <span>HyperGrowth Unicorn</span>
              </div>
              <div class="box pd-2 fs1-5 text-center mr-l5 mr-t2">
                  <span>Problem Solving at 10x Scale</span>
              </div>
              <div class="box pd-2 fs1-5 text-center mr-r5 mr-t2">
                  <span>International presence and Operation</span>
              </div>
              <div class="box pd-2 fs1-5 text-center mr-l5 mr-t2">
                  <span>Fasttrack career Advancement</span>
              </div>
              <div class="box pd-2 fs1-5 text-center mr-r5 mr-t2">
                  <span>Early opportunity to Lead</span>
              </div>
          </div>
      </div> 
      <div class="headline">
          <div class="leaderboard-heading-div display-flex flex-column heading-alignment">
              <span class="head-tagline fs1 mulish">What People Say</span>
              <span class="subtag-line">Sed ut perspiciatis unde omnis iste natuem  totam rem aperiam, eaque ipsarchitecto</span>
          </div>
          <div class="people-say-slider display-flex pd-b5">
                <div>
                    <div class="display-flex">
                        <div class="profile-img"></div>
                        <div>
                            <div class="people-say-card pd-2">
                             <q>Lenskart is not just another Unicorn, it's a rocketship. The speed and extent of</q>
                            </div>
                            <span>Priyanjit Biswas | Product Manager</span>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="display-flex">
                        <div class="profile-img"></div>
                        <div>
                            <div class="people-say-card pd-2">
                             <q>Lenskart is not just another Unicorn, it's a rocketship. The speed and extent of</q>
                            </div>
                            <span>Priyanjit Biswas | Product Manager</span>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="display-flex">
                        <div class="profile-img"></div>
                        <div>
                            <div class="people-say-card pd-2">
                             <q>Lenskart is not just another Unicorn, it's a rocketship. The speed and extent of</q>
                            </div>
                            <span>Priyanjit Biswas | Product Manager</span>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="display-flex">
                        <div class="profile-img"></div>
                        <div>
                            <div class="people-say-card pd-2">
                             <q>Lenskart is not just another Unicorn, it's a rocketship. The speed and extent of</q>
                            </div>
                            <span>Priyanjit Biswas | Product Manager</span>
                        </div>
                    </div>
                </div>
           </div>
      </div>
      <span class="section-divider bg-black text-center  pd-b2">
            ////////////////////////////////////////////////////////////////
        </span>
      <div class="headline">
          <div class="leaderboard-heading-div display-flex flex-column heading-alignment">
              <span class="head-tagline fs1 mulish">Check out our blogs</span>
              <span class="subtag-line">Lorem ipsum dolor sit amet consectetur adipisicing elite</span>
          </div>
      <div class="card-component display-flex justify-content-center pd-t2 pd-b2">
          <div class="card display-flex flex-column">
              <img class="card-img" src="../static//Rectangle 2542.svg" alt="an iphone on a macbook with dramatic lighting">
              <div class="card-content pd-2">
                  <h2 class="card-title">Technology</h2>
                  <span class="card-body">Using these 7 proven tips from experts in the industry</span>
              </div>
          </div>
          <div class="card display-flex flex-column">
              <img class="card-img" src="../static/Rectangle 2542 (1).svg" alt="an iphone on a macbook with dramatic lighting">
              <div class="card-content pd-2">
                  <h2 class="card-title">Culture</h2>
                  <span class="card-body">Using these 7 proven tips from experts in the industry</span>
              </div>
          </div>
          <div class="card display-flex flex-column">
              <img class="card-img" src="../static/Rectangle 2542 (2).svg" alt="an iphone on a macbook with dramatic lighting">
              <div class="card-content pd-2">
                  <h2 class="card-title">Design</h2>
                  <span class="card-body">Using these 7 proven tips from experts in the industry</span>
              </div>
          </div>
      </div>
      </div>
      </section>`
}

const navWrapper = document.getElementsByClassName('nav-wrapper');
document.addEventListener('scroll', (e) => {
    if(window.innerHeight + (window.innerHeight / 2) < window.scrollY) document.getElementsByClassName('nav-wrapper')[0].style.position = 'fixed';
    else document.getElementsByClassName('nav-wrapper')[0].style.position = 'sticky';
})

$('.people-say-slider').slick(
    {
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1,
	    dots: true,
	    arrows: false,
	    autoplay: true,
	    autoplaySpeed: 3000,
});
