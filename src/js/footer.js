onload();
function onload() {
    let footer = document.getElementById('footer');
    footer.innerHTML =
        `<footer class="footer-container pd-n pd-t5 pd-b1">
        <div class="footer-wrapper">
            <img class="footer-logo" alt="lenskart-logo" src="../static/lenskart-logo.svg">
            <div class="footer-content-wrapper display-flex justify-space-between mr-t2 mr-b7 mulish">
                <div class="footer-company-div">
                    <ul class="pd-0 pd-b1 fwBold">Company
                        <li class="mr-t2 fw400">About Us</li>
                        <li class="mr-t-5 fw400">Blog</li>
                        <li class="mr-t-5 fw400">Careers</li>
                    </ul>
                </div>
                <div class="footer-company-div">
                    <ul class="pd-0 pd-b1 fwBold">Company
                        <li class="mr-t2 fw400">About Us</li>
                        <li class="mr-t-5 fw400">Blog</li>
                        <li class="mr-t-5 fw400">Careers</li>
                    </ul>
                </div>

                <div class="right-footer-content">
                    <p class="">Download App</p>
                    <div class="download-links display-flex">
                        <img alt="play-store" class="link" src="../static/play-store.svg">
                        <img alt="app-store" class="link" src="../static/app-store.svg">
                    </div>
                    <p class="">Follow us on</p>
                    <span class="fs1"><u>Instagram</u> | <u>Facebook</u></span>
                </div>
            </div>
            <hr class="nav-division">
            <p>C 2010-2021 Lenskart Solutions Private Limited</p>
        </div>
    </footer>`
}